/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.samples.simple.mvp.view;


import com.shipdream.lib.ohos.mvc.MvcAbility;
import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.ResourceTable;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.controller.AppDelegateController;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.utils.PacMap;

public class AppDelegateFragment extends MvcAbility.DelegateFragment<AppDelegateController>
        implements AppDelegateController.View {
    private Text toolbarTitle;
    private Image toolbarBack;

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_fragment_app;
    }

    @Override
    protected int getContentLayoutResId() {
        return ResourceTable.Id_fragment_app_main;
    }

    @Override
    protected Class getControllerClass() {
        return AppDelegateController.class;
    }

    @Override
    public void update() {
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);

        toolbarTitle = (Text) view.findComponentById(ResourceTable.Id_toolbar_title);
        toolbarBack = (Image) view.findComponentById(ResourceTable.Id_toolbar_image);
        toolbarBack.setClickedListener(component -> controller.navigateBack(component));
    }

    /**
     * What to do when app starts for the first time
     */
    @Override
    protected void onStartUp() {
        controller.startApp(this);
    }

    @Override
    public void updateTitle(String title) {
        toolbarTitle.setText(title);
    }

    @Override
    public void changeNavIcon(boolean showBackArrow) {
        if (showBackArrow) {
            toolbarBack.setVisibility(Component.VISIBLE);
            //toolbar.setNavigationIcon(R.drawable.ic_action_nav_back);
        } else {
            toolbarBack.setVisibility(Component.HIDE);
        }
    }
}
