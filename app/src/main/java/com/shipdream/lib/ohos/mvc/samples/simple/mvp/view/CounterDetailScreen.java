/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.samples.simple.mvp.view;

import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.ResourceTable;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.controller.CounterDetailController;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.view.service.CountService;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.PacMap;


public class CounterDetailScreen extends AbstractFragment<CounterDetailController> {
    private Text display;

    @Override
    protected Class<CounterDetailController> getControllerClass() {
        return CounterDetailController.class;
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_screen_detail;
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        display = (Text) view.findComponentById(ResourceTable.Id_screen_detail_counterDisplay);
        view.findComponentById(ResourceTable.Id_screen_detail_buttonAutoIncrement)
                .setClickedListener(component -> startDemoAbility(CountService.class.getCanonicalName()));
        view.findComponentById(ResourceTable.Id_screen_detail_buttonIncrement)
                .setTouchEventListener(this::onTouchIncrement);
        view.findComponentById(ResourceTable.Id_screen_detail_buttonDecrement)
                .setTouchEventListener(this::onTouch);
    }

    private void startDemoAbility(String abilityName) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(operation);
        getFractionAbility().startAbility(intent);
    }

    /**
     * Controller will call update() whenever the controller thinks the state of the screen
     * changes. So just bind the state of the controller to this screen then the screen is always
     * reflecting the latest state/model of the controller
     */
    @Override
    public void update() {
        display.setText(controller.getCount());
    }


    private boolean onTouchIncrement(final Component component, TouchEvent event) {
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                controller.startContinuousIncrement();
                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                controller.stopContinuousIncrement();
                break;
        }
        return true;
    }

    private boolean onTouch(final Component component, TouchEvent event) {
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                controller.startContinuousDecrement();
                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                controller.stopContinuousDecrement();
                break;
        }
        return true;
    }

    @Override
    public boolean onBackButtonPressed() {
        //Use counterController to manage navigation back make navigation testable
        controller.goBackToBasicView(this);
        //Return true to not pass the back button pressed event to upper level handler.
        return true;
        //Or we can let the fragment manage back navigation back automatically where we don't
        //override this method which will call NavigationManager.navigateBack(Object sender)
        //automatically
    }
}
