package com.shipdream.lib.ohos.mvc.samples.simple.mvp;

import com.shipdream.lib.ohos.mvc.Mvc;
import com.shipdream.lib.poke.Provides;
import com.shipdream.lib.poke.exception.ProvideException;
import com.shipdream.lib.poke.exception.ProviderConflictException;
import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;

public class App extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        try {
            Mvc.graph().getRootComponent().register(new Object() {
                @Provides
                @AppContext
                public Context context() {
                    return getApplicationContext();
                }
            });
        } catch (ProvideException e) {
            e.printStackTrace();
        } catch (ProviderConflictException e) {
            e.printStackTrace();
        }
    }
}
