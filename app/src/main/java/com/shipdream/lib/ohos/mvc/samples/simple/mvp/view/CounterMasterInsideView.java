/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.samples.simple.mvp.view;


import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.ResourceTable;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.controller.CounterMasterInsideController;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.utils.PacMap;


public class CounterMasterInsideView extends AbstractFragment<CounterMasterInsideController> {
    private Text txtCountInEnglish;

    @Override
    protected Class<CounterMasterInsideController> getControllerClass() {
        return CounterMasterInsideController.class;
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_screen_master_subview;
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        txtCountInEnglish = (Text) view.findComponentById(ResourceTable.Id_screen_master_sub_countInEnglish);
    }

    @Override
    public void update() {
        txtCountInEnglish.setText(controller.getCountInEnglish());
    }
}
