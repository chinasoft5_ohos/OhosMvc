/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.samples.simple.mvp.view;


import com.shipdream.lib.ohos.mvc.Log;
import com.shipdream.lib.ohos.mvc.NavigationManager;
import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.Toast;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.ResourceTable;
import com.shipdream.lib.ohos.mvc.samples.simple.mvp.controller.CounterMasterController;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.BlendMode;
import ohos.global.resource.Resource;
import ohos.utils.PacMap;

import java.io.IOException;

import javax.inject.Inject;

public class CounterMasterScreen extends AbstractFragment<CounterMasterController>
        implements CounterMasterController.View {

    @Inject
    private NavigationManager navigationManager;

    private Text display;
    private Text ipValue;
    private RoundProgressBar ipProgress;

    @Override
    protected Class<CounterMasterController> getControllerClass() {
        return CounterMasterController.class;
    }

    /**
     * getLayoutResId.
     *
     * @return Layout id used to inflate the view of this MvcFragment.
     */
    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_screen_master;
    }


    /**
     * Lifecycle similar to onViewCreated by with more granular control with an extra argument to
     * indicate why this view is created: 1. first time created, or 2. rotated or 3. restored
     *
     * @param view               The root view of the fragment
     * @param savedInstanceState The savedInstanceState when the fragment is being recreated after
     *                           its enclosing ability is killed by OS, otherwise null including on
     *                           rotation
     * @param reason             Indicates the {@link Reason} why the onViewReady is called.
     */
    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        if (reason.isFirstTime()) {
            getFractionAbility().getUITaskDispatcher().delayDispatch(() -> {
                CounterMasterInsideView ff = new CounterMasterInsideView();
                getChildFragmentManager().beginTransaction()
                        .replace(ResourceTable.Id_screen_master_anotherFragmentContainer, ff)
                        .commit();
            }, 200);
        }
        display = (Text) view.findComponentById(ResourceTable.Id_screen_master_counterDisplay);
        ipValue = (Text) view.findComponentById(ResourceTable.Id_fragment_master_ipValue);
        ipProgress = (RoundProgressBar) view.findComponentById(ResourceTable.Id_fragment_master_ipProgress);
        view.findComponentById(ResourceTable.Id_screen_master_buttonIncrement)
                .setClickedListener(this::increment);
        view.findComponentById(ResourceTable.Id_screen_master_buttonDecrement)
                .setClickedListener(this::decrement);
        view.findComponentById(ResourceTable.Id_fragment_master_buttonShowDetailScreen)
                .setClickedListener(this::goToDetailPage);
        view.findComponentById(ResourceTable.Id_fragment_master_ipRefresh)
                .setClickedListener(this::refreshIp);
        Image ipRefresh = (Image) view.findComponentById(ResourceTable.Id_fragment_master_ipRefresh);
        Resource imageResource = null;
        try {
            imageResource = getFractionAbility()
                    .getResourceManager().getResource(ResourceTable.Media_stat_notify_sync);
        } catch (Exception e) {
            Log.i("hh", "error" + e.getMessage());
        }
        PixelMapElement pixelMapElement = new PixelMapElement(imageResource);
        pixelMapElement.setFilterPixelMap(true);
        getTintElement(pixelMapElement, 0xff999999);
        ipRefresh.setImageElement(pixelMapElement);
    }


    private Element getTintElement(Element element, int intColor) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[2][];
        states[0] = new int[]{0};
        states[1] = new int[]{0};
        element.setStateColorList(states, colors);
        element.setStateColorMode(BlendMode.SRC_ATOP);
        return element;
    }


    private void increment(Component component) {
        controller.increment(component);
    }

    private void decrement(Component component) {
        controller.decrement(component);
    }

    private void goToDetailPage(Component component) {
        controller.goToDetailScreen(component);
    }

    private void refreshIp(Component component) {
        controller.refreshIp();
    }

    @Override
    public void update() {
        display.setText(controller.getModel().getCount());
    }

    @Override
    public void showProgress() {
        ipProgress.setVisibility(Component.VISIBLE);
    }

    @Override
    public void hideProgress() {
        ipProgress.setVisibility(Component.INVISIBLE);
    }

    @Override
    public void updateIpValue(String ip) {
        ipValue.setVisibility(Component.VISIBLE);
        ipValue.setText(ip);
    }

    @Override
    public void showHttpError(int statusCode, String message) {
        Toast.showShort(getFractionAbility(), String.format("Http error(%d): %s", statusCode, message));
    }

    @Override
    public void showNetworkError(IOException e) {
        String string = getFractionAbility().getString(ResourceTable.String_network_error_to_get_ip);
        Toast.showShort(getFractionAbility(), string);
    }

}
