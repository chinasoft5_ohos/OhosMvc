/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * Navigation model state keeper implemented by ohos parcelable.
 */
public class NavigationModelKeeper implements OhosModelKeeper<NavigationManager.Model> {
    @Override
    public Sequenceable saveModel(NavigationManager.Model model, Class<NavigationManager.Model> modelType) {
        return new ModelParcelable(model);
    }

    @Override
    public NavigationManager.Model getModel(Sequenceable parceledModel, Class<NavigationManager.Model> modelType) {
        return ((ModelParcelable) parceledModel).model;
    }

    // Parcelable to manage navigation model
    public static class ModelParcelable implements Sequenceable {

        private NavigationManager.Model model;

        public static final Sequenceable.Producer<ModelParcelable> CREATOR
                = new Sequenceable.Producer<ModelParcelable>() {
            public ModelParcelable createFromParcel(Parcel in) {
                return new ModelParcelable(in);
            }

            public ModelParcelable[] newArray(int size) {
                return new ModelParcelable[size];
            }
        };

        private ModelParcelable(NavigationManager.Model model) {
            this.model = model;
        }

        private ModelParcelable(Parcel in) {
            unmarshalling(in);
        }

        private void readLocation(Parcel in, NavLocation curLoc, int end) {
            int pos = in.getReadPosition();
            if (pos < end) {
                String str = in.readString();
                NavLocation location = new NavLocation();
                location._setLocationId(str);
                curLoc._setPreviousLocation(location);
                readLocation(in, location, end);
            }
        }


        private void writeLocation(Parcel dest, NavLocation location) {
            if (location != null) {
                dest.writeString(location.getLocationId());
                writeLocation(dest, location.getPreviousLocation());
            }
        }

        @Override
        public boolean marshalling(Parcel dest) {
            int start = dest.getReadPosition();
            dest.writeInt(0);
            writeLocation(dest, model.getCurrentLocation());
            int end = dest.getReadPosition();
            // Rewind back and write the size of the parcel
            int size = end - start;
            dest.rewindWrite(start);
            dest.writeInt(size);
            // Move the cursor back
            dest.rewindWrite(end);
            return true;
        }

        @Override
        public boolean unmarshalling(Parcel in) {
            model = new NavigationManager.Model();
            int start = in.getReadPosition();
            int size = in.readInt();
            int end = start + size;
            if (in.getReadPosition() < end) {
                String locId = in.readString();
                NavLocation location = new NavLocation();
                location._setLocationId(locId);
                model.setCurrentLocation(location);
                readLocation(in, location, end);
            }
            return true;
        }
    }

}
