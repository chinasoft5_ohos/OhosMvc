/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shipdream.lib.ohos.mvc;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.logging.Logger;

/**
 * log .
 *
 * @author a
 * @version v
 */
public class Log {
    private static final String TAG_LOG = "LogUtil";
    private static final String TAG = "LOG";
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(0, 0, Log.TAG_LOG);
    private static final String LOG_FORMAT = "%{public}s: %{public}s";
    private static final String LOG_FORMAT2 = "%{public}s: %{public}s: %{public}s";

    /**
     * info.
     *
     * @param msg
     * @return int
     */
    public static int info(String msg) {
        return HiLog.info(LABEL_LOG, LOG_FORMAT, TAG, msg);
    }

    /**
     * d .
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    public static int d(String tag, String msg) {
        logInfo(tag, msg);
        return HiLog.debug(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    /**
     * d.
     *
     * @param tag tag
     * @param msg msg
     * @param tr  tr
     * @return int
     */
    public static int d(String tag, String msg, Throwable tr) {
        return HiLog.debug(LABEL_LOG, LOG_FORMAT2, tag, msg, HiLog.getStackTrace(tr));
    }

    /**
     * i.
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    public static int i(String tag, String msg) {
        logInfo(tag, msg);
        return HiLog.info(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    /**
     * e.
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    public static int e(String tag, String msg) {
        return HiLog.error(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    /**
     * e.
     *
     * @param tag tag
     * @param msg msg
     * @param tr  tr
     * @return int
     */
    public static int e(String tag, String msg, Throwable tr) {
        return HiLog.error(LABEL_LOG, LOG_FORMAT2, tag, msg, HiLog.getStackTrace(tr));
    }

    private static void logInfo(String tag, String msg) {
        Logger.getLogger(tag).info(msg);
    }

}


