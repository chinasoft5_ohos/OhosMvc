/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc;


import ohos.utils.Sequenceable;

/**
 * Implements this interface to use {@link Sequenceable} to save and restore state with better
 * performance
 *
 * @param <T> T
 */
public interface OhosModelKeeper<T> {
    /**
     * Save the given state into {@link Sequenceable}
     *
     * @param model     the model to save
     * @param modelType modelType
     * @return Sequenceable
     */
    Sequenceable saveModel(T model, Class<T> modelType);

    /**
     * Restore state from the {@link Sequenceable}
     *
     * @param parceledModel the model to save
     * @param modelType     modelType
     * @return The restored model
     */
    T getModel(Sequenceable parceledModel, Class<T> modelType);
}
