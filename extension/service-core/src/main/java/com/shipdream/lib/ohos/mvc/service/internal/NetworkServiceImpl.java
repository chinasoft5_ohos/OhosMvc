/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.service.internal;

import com.shipdream.lib.ohos.mvc.service.NetworkService;
import ohos.app.Context;
import ohos.net.NetCapabilities;
import ohos.net.NetManager;

/**
 *
 */
public class NetworkServiceImpl implements NetworkService {
    private Context context;

    public NetworkServiceImpl(Context context) {
        this.context = context;
    }

    @Override
    public NetworkStatus getCurrentNetworkStatus() {
        NetManager cm = NetManager.getInstance(context);
        NetCapabilities netCapabilities = cm.getNetCapabilities(cm.getDefaultNet());

        if (null != netCapabilities) {
            if (netCapabilities.hasCap(NetCapabilities.NET_CAPABILITY_VALIDATED)
                    && netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI)
                    || netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI_AWARE)) {
                return NetworkStatus.WIFI;
            }
            if (netCapabilities.hasCap(NetCapabilities.NET_CAPABILITY_VALIDATED)
                    && netCapabilities.hasBearer(NetCapabilities.BEARER_CELLULAR)) {
                return NetworkStatus.MOBILE;
            }
        }
        return NetworkStatus.NOT_CONNECTED;
    }


}
