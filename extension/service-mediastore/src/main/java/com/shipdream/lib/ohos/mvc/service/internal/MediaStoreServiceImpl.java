/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.service.internal;


import com.shipdream.lib.ohos.mvc.service.MediaStoreService;
import com.shipdream.lib.ohos.mvc.service.dto.ImageDTO;
import com.shipdream.lib.ohos.mvc.service.dto.ThumbnailDTO;
import com.shipdream.lib.ohos.mvc.service.dto.VideoDTO;
import com.shipdream.lib.ohos.mvc.service.dto.VisualMediaDTO;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.ability.DataUriUtils;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FilenameFilter;
import java.util.*;

/**
 * Media store service to use real ohos media store API
 */
public class MediaStoreServiceImpl implements MediaStoreService {
    public static final int ORIENTATION_NORMAL = 1;
    public static final int ORIENTATION_FLIP_HORIZONTAL = 2;
    public static final int ORIENTATION_ROTATE_180 = 3;
    public static final int ORIENTATION_FLIP_VERTICAL = 4;
    public static final int ORIENTATION_TRANSPOSE = 5;
    public static final int ORIENTATION_ROTATE_90 = 6;
    public static final int ORIENTATION_TRANSVERSE = 7;
    public static final int ORIENTATION_ROTATE_270 = 8;
    public static final java.lang.String DESCRIPTION = "description";
    public static final java.lang.String PICASA_ID = "picasa_id";
    public static final java.lang.String IS_PRIVATE = "isprivate";
    public static final java.lang.String LATITUDE = "latitude";
    public static final java.lang.String LONGITUDE = "longitude";
    public static final java.lang.String DATE_TAKEN = "datetaken";
    public static final java.lang.String ORIENTATION = "orientation";
    public static final java.lang.String MINI_THUMB_MAGIC = "mini_thumb_magic";
    public static final java.lang.String BUCKET_ID = "bucket_id";
    public static final java.lang.String BUCKET_DISPLAY_NAME = "bucket_display_name";
    public static final java.lang.String WIDTH = "width";
    public static final java.lang.String HEIGHT = "height";
    public static final java.lang.String IMAGE_ID = "image_id";
    public static final java.lang.String KIND = "kind";
    public static final java.lang.String RESOLUTION = "resolution";
    public static final java.lang.String ARTIST = "artist";
    public static final java.lang.String ALBUM = "album";
    public static final java.lang.String VIDEO_ID = "video_id";
    public static final int MINI_KIND = 1;
    public static final int FULL_SCREEN_KIND = 2;
    public static final int MICRO_KIND = 3;

    private static final String TAG = MediaStoreServiceImpl.class.getName();
    public static final String MediaStore_Media_WIDTH = "width";
    public static final String MediaStore_Media_HEIGHT = "height";
    protected Context context;
    private HiLogLabel LABEL_LOG = new HiLogLabel(0, 0, "LogUtil");
    private String LOG_FORMAT = "%{public}s: %{public}s";

    public MediaStoreServiceImpl(Context context) {
        this.context = context;
    }

    /**
     * Matches code in MediaProvider.computeBucketValues. Should be a common
     * function.
     *
     * @param path path
     * @return String
     */
    public String getBucketId(String path) {
        return String.valueOf(path.toLowerCase(Locale.US).hashCode());
    }

    public String[] getDicmBuckets() {
        String[] dcimBuckets;
        final String root = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        java.io.File dcim = new java.io.File(root);
        if (dcim.exists()) {
            String[] subFolders = dcim.list(new FilenameFilter() {
                @Override
                public boolean accept(java.io.File dir, String name) {
                    return dir.isDirectory() && !name.startsWith(".");
                }
            });

            if (subFolders.length == 0) {
                dcimBuckets = new String[]{getBucketId(root)};
            } else {
                dcimBuckets = new String[subFolders.length];
                for (int i = 0; i < subFolders.length; i++) {
                    dcimBuckets[i] = getBucketId(root + "/" + subFolders[i]);
                }
            }
        } else {
            dcimBuckets = new String[0];
        }
        return dcimBuckets;
    }

    @Override
    public VisualMediaDTO getLatestVisualMedia() {

        List<ImageDTO> images = extractImagesFromCursor(
                getAllBucketImagesCursor(null,
                        DATE_TAKEN + " DESC"), 0, 1);
        List<VideoDTO> videos = extractVideosFromCursor(
                getAllBucketVideosCursor(null,
                        DATE_TAKEN + " DESC"), 0, 1);
        ImageDTO lastImage = null;
        if (images.size() > 0) {
            lastImage = images.get(0);
        }
        VideoDTO lastVideo = null;
        if (videos.size() > 0) {
            lastVideo = videos.get(0);
        }

        if (lastImage == null && lastVideo == null) {
            return null;
        } else {
            if (lastImage != null && lastVideo != null) {
                if (lastImage.getTakenDate().getTime() > lastVideo.getTakenDate().getTime()) {
                    return lastImage;
                } else {
                    return lastVideo;
                }
            } else if (lastImage != null) {
                return lastImage;
            } else {
                return lastVideo;
            }
        }
    }

    public Map<String, List<ImageDTO>> getDcimImages() {
        Map<String, List<ImageDTO>> result = new LinkedHashMap<>();
        String[] imageBuckets = getDicmBuckets();
        for (int i = 0; i < imageBuckets.length; i++) {
            String bucket = imageBuckets[i];
            result.put(bucket, getImages(bucket, 0, 0));
        }
        return result;
    }

    public int getDcimImageCount() {
        int count = 0;
        String[] imageBuckets = getDicmBuckets();
        for (int i = 0; i < imageBuckets.length; i++) {
            ResultSet cursor = getAllBucketImagesCursor(imageBuckets[i], null);
            if (cursor != null) {
                count += cursor.getRowCount();
                cursor.close();
            }
        }
        return count;
    }

    private String[] imageProjection = null;

    protected String[] getImageProjection() {
        if (imageProjection == null) {

            String[] projection = {AVStorage.Images.Media.ID,
                    AVStorage.Images.Media.TITLE,
                    AVStorage.Images.Media.DISPLAY_NAME,
                    DESCRIPTION,
                    BUCKET_ID,
                    BUCKET_DISPLAY_NAME,
                    AVStorage.Images.Media.DATA,
                    AVStorage.Images.Media.MIME_TYPE,
                    AVStorage.Images.Media.SIZE,
                    ORIENTATION,
                    AVStorage.Images.Media.DATE_ADDED,
                    AVStorage.Images.Media.DATE_MODIFIED,
                    DATE_TAKEN,
                    LATITUDE,
                    LONGITUDE};

            // Added two columns for SDK > 16
            int len = projection.length + 2;
            String[] projection16 = new String[projection.length + 2];
            for (int i = 0; i < projection.length; i++) {
                projection16[i] = projection[i];
            }
            projection16[len - 2] = MediaStore_Media_WIDTH;
            projection16[len - 1] = MediaStore_Media_HEIGHT;

            projection = projection16;

            imageProjection = projection;
        }
        return imageProjection;
    }

    private ResultSet getAllBucketImagesCursor(String bucketId, String orderBy) {
        String selection = BUCKET_ID + " = ?";
        String[] selectionArgs = {bucketId};
        if (bucketId == null) {
            selection = null;
            selectionArgs = null;
        }
        DataAbilityHelper helper = DataAbilityHelper.creator(context);
        DataAbilityPredicates dataAbilityPredicates = new DataAbilityPredicates(selection);
        dataAbilityPredicates.setWhereArgs(Arrays.asList(selectionArgs));
        dataAbilityPredicates.setOrder(orderBy);

        ResultSet cursor = null;
        try {
            cursor = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI,
                    getImageProjection(), dataAbilityPredicates);
        } catch (DataAbilityRemoteException e) {
            log(TAG, "getAllBucketImagesCursor " + e.getMessage());
        }

        if (cursor == null) {
            log(TAG, "Failed to get cursor of all images of bucket " + bucketId);
        }
        return cursor;
    }

    @Override
    public List<ImageDTO> getImages(String bucketId, int offset, int limit) {
        return extractImagesFromCursor(getAllBucketImagesCursor(bucketId, null), offset, limit);
    }

    private int imageIdCol = -1;
    private int imageTitleCol;
    private int imageDisplayNameCol;
    private int imageDescriptionCol;
    private int imageBucketIdCol;
    private int imageBucketDisplayNameCol;
    private int imageDataCol;
    private int imageMimeCol;
    private int imageSizeCol;
    private int imageOrientationCol;
    private int imageDateAddedCol;
    private int imageDateTakenCol;
    private int imageDateModifyCol;
    private int latitudeCol;
    private int longitudeCol;
    private int widthCol = -1;
    private int heightCol = -1;

    /**
     * Extract an imageDTO from given cursor position from its current position.
     *
     * @param cursor cursor
     * @return ImageDTO
     */
    protected ImageDTO extractOneImageFromCurrentCursor(ResultSet cursor) {
        if (imageIdCol == -1) {
            imageIdCol = cursor.getColumnIndexForName(AVStorage.Images.Media.ID);
            imageTitleCol = cursor.getColumnIndexForName(AVStorage.Images.Media.TITLE);
            imageDisplayNameCol = cursor.getColumnIndexForName(AVStorage.Images.Media.DISPLAY_NAME);
            imageDescriptionCol = cursor.getColumnIndexForName(DESCRIPTION);
            imageBucketIdCol = cursor.getColumnIndexForName(BUCKET_ID);
            imageBucketDisplayNameCol = cursor.getColumnIndexForName(BUCKET_DISPLAY_NAME);
            imageDataCol = cursor.getColumnIndexForName(AVStorage.Images.Media.DATA);
            imageMimeCol = cursor.getColumnIndexForName(AVStorage.Images.Media.MIME_TYPE);
            imageSizeCol = cursor.getColumnIndexForName(AVStorage.Images.Media.SIZE);
            imageOrientationCol = cursor.getColumnIndexForName(ORIENTATION);
            imageDateAddedCol = cursor.getColumnIndexForName(AVStorage.Images.Media.DATE_ADDED);
            imageDateTakenCol = cursor.getColumnIndexForName(DATE_TAKEN);
            imageDateModifyCol = cursor.getColumnIndexForName(AVStorage.Images.Media.DATE_MODIFIED);
            latitudeCol = cursor.getColumnIndexForName(LATITUDE);
            longitudeCol = cursor.getColumnIndexForName(LONGITUDE);
            widthCol = cursor.getColumnIndexForName(MediaStore_Media_WIDTH);
            heightCol = cursor.getColumnIndexForName(MediaStore_Media_HEIGHT);
        }

        ImageDTO image = new ImageDTO();
        image.setId(cursor.getLong(imageIdCol));
        image.setTitle(cursor.getString(imageTitleCol));
        image.setDisplayName(cursor.getString(imageDisplayNameCol));
        image.setDescription(cursor.getString(imageDescriptionCol));
        image.setBucketId(cursor.getString(imageBucketIdCol));
        image.setBucketDisplayName(cursor.getString(imageBucketDisplayNameCol));
        image.setUri(cursor.getString(imageDataCol));
        image.setMimeType(cursor.getString(imageMimeCol));
        image.setSize(cursor.getLong(imageSizeCol));
        image.setOrientation(translateOrientation(cursor.getInt(imageOrientationCol)));
        image.setAddedDate(new Date(cursor.getLong(imageDateAddedCol)));
        image.setTakenDate(new Date(cursor.getLong(imageDateTakenCol)));
        image.setModifyDate(new Date(cursor.getLong(imageDateModifyCol)));
        image.setLatitude(cursor.getDouble(latitudeCol));
        image.setLongitude(cursor.getDouble(longitudeCol));
        image.setWidth(cursor.getInt(widthCol));
        image.setHeight(cursor.getInt(heightCol));

        return image;
    }

    /**
     * Extract a list of imageDTO from current cursor with the given offset and limit.
     *
     * @param cursor cursor
     * @param offset offset
     * @param limit  limit
     * @return List<ImageDTO>
     */
    protected List<ImageDTO> extractImagesFromCursor(ResultSet cursor, int offset, int limit) {
        List<ImageDTO> images = new ArrayList<>();
        int count = 0;
        int begin = offset > 0 ? offset : 0;
        if (cursor.goToRow(begin)) {
            do {
                ImageDTO image = extractOneImageFromCurrentCursor(cursor);
                images.add(image);
                count++;
                if (limit > 0 && count > limit) {
                    break;
                }
            } while (cursor.goToNextRow());
        }
        cursor.close();

        return images;
    }

    public String getImageUriById(String imageId) {
        final String[] projection = {AVStorage.Images.Media.ID, AVStorage.Images.Media.DATA};
        final String selection = AVStorage.Images.Media.ID + " = ?";
        final String[] selectionArgs = {imageId};
        DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);
        DataAbilityPredicates predicates = new DataAbilityPredicates(selection);
        predicates.setWhereArgs(Arrays.asList(selectionArgs));

        ResultSet cursor = null;
        try {
            cursor = dataAbilityHelper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI,
                    projection, predicates);
        } catch (DataAbilityRemoteException e) {
            log(TAG, "getImageUriById " + e.getMessage());
        }
        if (cursor != null) {
            final int dataCol = cursor.getColumnIndexForName(AVStorage.Images.Media.DATA);
            String imageUrl = null;
            if (cursor.goToFirstRow()) {
                do {
                    imageUrl = cursor.getString(dataCol);
                } while (cursor.goToNextRow());
            }
            cursor.close();
            return imageUrl;
        } else {
            return null;
        }
    }

    @Override
    public ThumbnailDTO getImageThumbnailById(long imageId, ThumbnailDTO.Kind kind) {

        final String[] projection = {
                AVStorage.AVBaseColumns.DATA,
                WIDTH,
                HEIGHT,
        };
        final String selection = IMAGE_ID + " = ? AND "
                + KIND + " = ?";
        String kindInString = null;
        switch (kind) {
            case MICRO:
                kindInString = Integer.toString(MICRO_KIND);
                break;
            case MINI:
                kindInString = Integer.toString(MINI_KIND);
                break;
            case FULL_SCREEN:
                kindInString = Integer.toString(FULL_SCREEN_KIND);
        }
        final String[] selectionArgs = {String.valueOf(imageId), kindInString};

        ThumbnailDTO thumbnail = null;

        DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);

        DataAbilityPredicates predicates = new DataAbilityPredicates(selection);
        predicates.setWhereArgs(Arrays.asList(selectionArgs));

        ResultSet cursor = null;
        try {
            cursor = dataAbilityHelper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI,
                    projection, predicates);
        } catch (DataAbilityRemoteException e) {
            log(TAG, "getImageThumbnailById " + e.getMessage());
        }
        try {
            if (cursor != null && cursor.getRowCount() > 0) {
                thumbnail = new ThumbnailDTO();
                final int dataCol = cursor.getColumnIndexForName(AVStorage.AVBaseColumns.DATA);
                final int widthCol = cursor.getColumnIndexForName(WIDTH);
                final int heightCol = cursor.getColumnIndexForName(HEIGHT);
                if (cursor.goToFirstRow()) {
                    thumbnail.setOriginalFileId(imageId);
                    thumbnail.setUri(cursor.getString(dataCol));
                    thumbnail.setWidth(cursor.getInt(widthCol));
                    thumbnail.setHeight(cursor.getInt(heightCol));
                    thumbnail.setKind(kind);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return thumbnail;
    }

    public Map<String, List<VideoDTO>> getDcimVideos() {
        Map<String, List<VideoDTO>> result = new LinkedHashMap<>();
        String[] videoBuckets = getDicmBuckets();
        for (int i = 0; i < videoBuckets.length; i++) {
            String bucket = videoBuckets[i];
            result.put(bucket, getVideos(bucket, 0, 0));
        }
        return result;
    }

    @Override
    public int getDcimVideosCount() {
        int count = 0;
        String[] videoBuckets = getDicmBuckets();
        for (int i = 0; i < videoBuckets.length; i++) {
            ResultSet cursor = getAllBucketVideosCursor(videoBuckets[i], null);
            if (cursor != null) {
                count += cursor.getRowCount();
                cursor.close();
            }
        }
        return count;
    }

    private String[] videoProjection;

    protected String[] getVideoProjection() {
        if (videoProjection == null) {
            String[] projection = {AVStorage.Video.Media.ID,
                    AVStorage.Video.Media.TITLE,
                    AVStorage.Video.Media.DISPLAY_NAME,
                    DESCRIPTION,
                    BUCKET_ID,
                    BUCKET_DISPLAY_NAME,
                    AVStorage.Video.Media.DATA,
                    AVStorage.Video.Media.MIME_TYPE,
                    RESOLUTION,
                    AVStorage.Video.Media.SIZE,
                    AVStorage.Video.Media.DATE_ADDED,
                    AVStorage.Video.Media.DATE_MODIFIED,
                    DATE_TAKEN,
                    LATITUDE,
                    LONGITUDE,
                    ALBUM,
                    ARTIST};
            videoProjection = projection;
        }
        return videoProjection;
    }

    private ResultSet getAllBucketVideosCursor(String bucketId, String orderBy) {

        String selection = BUCKET_ID + " = ?";
        String[] selectionArgs = {bucketId};
        if (bucketId == null) {
            selection = null;
            selectionArgs = null;
        }

        DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);
        DataAbilityPredicates predicates = new DataAbilityPredicates(selection);
        predicates.setWhereArgs(Arrays.asList(selectionArgs));
        predicates.setOrder(orderBy);
        ResultSet cursor = null;
        try {
            cursor = dataAbilityHelper.query(AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI,
                    getVideoProjection(), predicates);
        } catch (DataAbilityRemoteException e) {
            log(TAG, "getAllBucketVideosCursor " + e.getMessage());
        }
        if (cursor == null) {
            log(TAG, "Failed to get cursor of all video of bucket " + bucketId);
        }
        return cursor;
    }


    @Override
    public List<VideoDTO> getVideos(String bucketId, int offset, int limit) {
        return extractVideosFromCursor(getAllBucketVideosCursor(bucketId, null), offset, limit);
    }

    private int videoIdCol = -1;
    private int videoTitleCol;
    private int videoDisplayNameCol;
    private int videoDescriptionCol;
    private int videoBucketIdCol;
    private int videoBucketDisplayNameCol;
    private int videoDataCol;
    private int videoMimeCol;
    private int videoResolutionCol;
    private int videoSizeCol;
    private int videoDateAddedCol;
    private int videoDateTakenCol;
    private int videoDateModifyCol;
    private int videoLatitudeCol;
    private int videoLongitudeCol;
    private int videoAlbumCol;
    private int videoArtistCol;

    /**
     * Extract one videoDTO from the given cursor from its current position
     *
     * @param cursor cursor
     * @return VideoDTO
     */
    protected VideoDTO extractOneVideoFromCursor(ResultSet cursor) {
        if (videoIdCol == -1) {
            videoIdCol = cursor.getColumnIndexForName(AVStorage.Video.Media.ID);
            videoTitleCol = cursor.getColumnIndexForName(AVStorage.Video.Media.TITLE);
            videoDisplayNameCol = cursor.getColumnIndexForName(AVStorage.Video.Media.DISPLAY_NAME);
            videoDescriptionCol = cursor.getColumnIndexForName(DESCRIPTION);
            videoBucketIdCol = cursor.getColumnIndexForName(BUCKET_ID);
            videoBucketDisplayNameCol = cursor.getColumnIndexForName(BUCKET_DISPLAY_NAME);
            videoDataCol = cursor.getColumnIndexForName(AVStorage.Video.Media.DATA);
            videoMimeCol = cursor.getColumnIndexForName(AVStorage.Video.Media.MIME_TYPE);
            videoResolutionCol = cursor.getColumnIndexForName(RESOLUTION);
            videoSizeCol = cursor.getColumnIndexForName(AVStorage.Video.Media.SIZE);
            videoDateAddedCol = cursor.getColumnIndexForName(AVStorage.Video.Media.DATE_ADDED);
            videoDateTakenCol = cursor.getColumnIndexForName(DATE_TAKEN);
            videoDateModifyCol = cursor.getColumnIndexForName(AVStorage.Video.Media.DATE_MODIFIED);
            videoLatitudeCol = cursor.getColumnIndexForName(LATITUDE);
            videoLongitudeCol = cursor.getColumnIndexForName(LONGITUDE);
            videoAlbumCol = cursor.getColumnIndexForName(ALBUM);
            videoArtistCol = cursor.getColumnIndexForName(ARTIST);
        }

        VideoDTO video = new VideoDTO();
        video.setId(cursor.getLong(videoIdCol));
        video.setTitle(cursor.getString(videoTitleCol));
        video.setDisplayName(cursor.getString(videoDisplayNameCol));
        video.setDescription(cursor.getString(videoDescriptionCol));
        video.setBucketId(cursor.getString(videoBucketIdCol));
        video.setBucketDisplayName(cursor.getString(videoBucketDisplayNameCol));
        video.setUri(cursor.getString(videoDataCol));
        video.setMimeType(cursor.getString(videoMimeCol));
        video.setSize(cursor.getLong(videoSizeCol));
        video.setAddedDate(new Date(cursor.getLong(videoDateAddedCol)));
        video.setTakenDate(new Date(cursor.getLong(videoDateTakenCol)));
        video.setModifyDate(new Date(cursor.getLong(videoDateModifyCol)));
        video.setLatitude(cursor.getDouble(videoLatitudeCol));
        video.setLongitude(cursor.getDouble(videoLongitudeCol));
        video.setAlbum(cursor.getString(videoAlbumCol));
        video.setArtist(cursor.getString(videoArtistCol));
        String resolution = cursor.getString(videoResolutionCol);
        if (resolution != null) {
            try {
                String[] res = resolution.split("x");
                int width = Integer.parseInt(res[0]);
                int height = Integer.parseInt(res[1]);
                video.setWidth(width);
                video.setHeight(height);
            } catch (Exception e) {
                log(TAG, String.format("Failed to parse resolution of video(id=%d, title=%s, displayName=%s)",
                        video.getId(), video.getTitle(), video.getDisplayName()));
            }

        }
        return video;
    }

    /**
     * Extract a list of videoDTO from current cursor with the given offset and limit.
     *
     * @param cursor cursor
     * @param offset offset
     * @param limit limit
     * @return List<VideoDTO>
     */
    protected List<VideoDTO> extractVideosFromCursor(ResultSet cursor, int offset, int limit) {
        List<VideoDTO> videos = new ArrayList<>();
        int count = 0;
        int begin = offset > 0 ? offset : 0;
        if (cursor.goToRow(begin)) {
            do {
                VideoDTO video = extractOneVideoFromCursor(cursor);
                videos.add(video);
                count++;
                if (limit > 0 && count > limit) {
                    break;
                }
            } while (cursor.goToNextRow());
        }
        cursor.close();

        return videos;
    }

    public String getVideoUriById(String videoId) {
        final String[] projection = {AVStorage.Video.Media.ID, AVStorage.Video.Media.DATA};
        final String selection = AVStorage.Video.Media.ID + " = ?";
        final String[] selectionArgs = {videoId};
        DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);

        DataAbilityPredicates predicates = new DataAbilityPredicates(selection);
        predicates.setWhereArgs(Arrays.asList(selectionArgs));
        ResultSet cursor = null;
        try {
            cursor = dataAbilityHelper.query(AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI,
                    projection, predicates);
        } catch (DataAbilityRemoteException e) {
            log(TAG, "getVideoUriById " + e.getMessage());
        }

        if (cursor == null) {
            return null;
        }

        final int dataCol = cursor.getColumnIndexForName(AVStorage.Video.Media.DATA);
        String videoUrl = null;
        if (cursor.goToFirstRow()) {
            do {
                videoUrl = cursor.getString(dataCol);
            } while (cursor.goToNextRow());
        }
        cursor.close();
        return videoUrl;
    }

    @Override
    public ThumbnailDTO getVideoThumbnailById(long videoId, ThumbnailDTO.Kind kind) {
        final String[] projection = {
                AVStorage.AVBaseColumns.DATA,
                WIDTH,
                HEIGHT,
        };
        final String selection = VIDEO_ID + " = ? AND "
                + KIND + " = ?";
        String kindInString = null;
        switch (kind) {
            case MICRO:
                kindInString = Integer.toString(MICRO_KIND);
                break;
            case MINI:
                kindInString = Integer.toString(MINI_KIND);
                break;
            case FULL_SCREEN:
                kindInString = Integer.toString(FULL_SCREEN_KIND);
        }
        final String[] selectionArgs = {String.valueOf(videoId), kindInString};

        ThumbnailDTO thumbnail = null;

        DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);

        DataAbilityPredicates predicates = new DataAbilityPredicates(selection);
        predicates.setWhereArgs(Arrays.asList(selectionArgs));
        ResultSet cursor = null;
        try {
            cursor = dataAbilityHelper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI,
                    projection, predicates);
        } catch (DataAbilityRemoteException e) {
            log(TAG, "getVideoUriById " + e.getMessage());
        }
        try {
            if (cursor != null && cursor.getRowCount() > 0) {
                thumbnail = new ThumbnailDTO();
                final int dataCol = cursor.getColumnIndexForName(AVStorage.AVBaseColumns.DATA);
                final int widthCol = cursor.getColumnIndexForName(WIDTH);
                final int heightCol = cursor.getColumnIndexForName(HEIGHT);
                if (cursor.goToFirstRow()) {
                    thumbnail.setOriginalFileId(videoId);
                    thumbnail.setUri(cursor.getString(dataCol));
                    thumbnail.setWidth(cursor.getInt(widthCol));
                    thumbnail.setHeight(cursor.getInt(heightCol));
                    thumbnail.setKind(kind);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return thumbnail;
    }

    @Override
    public void deleteFiles(long[] deletingFileIds) {
        for (int i = 0; i < deletingFileIds.length; i++) {
            Uri uri = DataUriUtils.attachId(AVStorage.Audio.Media.EXTERNAL_DATA_ABILITY_URI, deletingFileIds[i]);

            DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);
            try {
                dataAbilityHelper.delete(uri, null);
            } catch (DataAbilityRemoteException e) {
                log(TAG, "deleteFiles id " + deletingFileIds[i] + e.getMessage());
            }
        }
    }


    private ImageDTO.Orientation translateOrientation(int orientation) {
        switch (orientation) {
            case ORIENTATION_FLIP_HORIZONTAL:
                return ImageDTO.Orientation.FLIP_HORIZONTAL;
            case ORIENTATION_FLIP_VERTICAL:
                return ImageDTO.Orientation.FLIP_VERTICAL;
            case ORIENTATION_NORMAL:
                return ImageDTO.Orientation.NORMAL;
            case ORIENTATION_ROTATE_90:
                return ImageDTO.Orientation.ROTATE_90;
            case ORIENTATION_ROTATE_180:
                return ImageDTO.Orientation.ROTATE_180;
            case ORIENTATION_ROTATE_270:
                return ImageDTO.Orientation.ROTATE_270;
            case ORIENTATION_TRANSPOSE:
                return ImageDTO.Orientation.TRANSPOSE;
            case ORIENTATION_TRANSVERSE:
                return ImageDTO.Orientation.TRANSVERSE;
            default:
                return ImageDTO.Orientation.UNDEFINED;
        }
    }


    private void log(String tag, String msg) {
        HiLog.info(LABEL_LOG, LOG_FORMAT, tag, msg);
    }
}
