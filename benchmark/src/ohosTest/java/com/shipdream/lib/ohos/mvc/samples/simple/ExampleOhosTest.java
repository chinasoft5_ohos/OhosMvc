/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shipdream.lib.ohos.mvc.samples.simple;

import com.shipdream.lib.ohos.mvc.MvcGraph;
import com.shipdream.lib.ohos.mvc.samples.simple.controller.*;
import com.shipdream.lib.ohos.mvc.samples.simple.controller.internal.*;
import com.shipdream.lib.ohos.mvc.samples.simple.service.*;
import com.shipdream.lib.ohos.mvc.samples.simple.service.internal.*;
import com.shipdream.lib.poke.ProviderByClassType;
import com.shipdream.lib.poke.exception.ProviderConflictException;
import org.junit.Test;

/**
 * ExampleOhosTest .
 *
 * @author a
 * @version v
 */
public class ExampleOhosTest {
    /**
     * testFunc1.
     */
    @Test
    public void testFunc1() {
        MainAbility.Container10 container = new MainAbility.Container10();
        ControllerComponent comp = DaggerControllerComponent.builder()
                .controllerModule(new ControllerModule()).build();
        comp.inject(container);
    }

    /**
     * testFunc1.
     */
    @Test
    public void testFunc2() {
        MainAbility.Container10x10 container = new MainAbility.Container10x10();
        ControllerComponent comp = DaggerControllerComponent.builder()
                .controllerModule(new ControllerModule()).build();
        comp.inject(container);
        comp.inject((Controller0Impl) container.controller0);
        comp.inject((Controller1Impl) container.controller1);
        comp.inject((Controller2Impl) container.controller2);
        comp.inject((Controller3Impl) container.controller3);
        comp.inject((Controller4Impl) container.controller4);
        comp.inject((Controller5Impl) container.controller5);
        comp.inject((Controller6Impl) container.controller6);
        comp.inject((Controller7Impl) container.controller7);
        comp.inject((Controller8Impl) container.controller8);
        comp.inject((Controller9Impl) container.controller9);

    }

    private void register(MvcGraph graph, Class type, Class impl) throws ProviderConflictException {
        graph.getRootComponent().register(new ProviderByClassType(type, impl));
    }

    /**
     * testFunc1.
     */
    @Test
    public void testFunc3() {
        MainAbility.Container10 container = new MainAbility.Container10();

        try {
            MvcGraph graph = new MvcGraph();

            register(graph, Service0.class, Service0Impl.class);
            register(graph, Service1.class, Service1Impl.class);
            register(graph, Service2.class, Service2Impl.class);
            register(graph, Service3.class, Service3Impl.class);
            register(graph, Service4.class, Service4Impl.class);
            register(graph, Service5.class, Service5Impl.class);
            register(graph, Service6.class, Service6Impl.class);
            register(graph, Service7.class, Service7Impl.class);
            register(graph, Service8.class, Service8Impl.class);
            register(graph, Service9.class, Service9Impl.class);

            graph.inject(container);

        } catch (ProviderConflictException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * testFunc1.
     */
    @Test
    public void testFunc4() {
        MainAbility.Container10x10 container = new MainAbility.Container10x10();

        try {
            MvcGraph graph = new MvcGraph();
            register(graph, Controller0.class, Controller0Impl.class);
            register(graph, Controller1.class, Controller1Impl.class);
            register(graph, Controller2.class, Controller2Impl.class);
            register(graph, Controller3.class, Controller3Impl.class);
            register(graph, Controller4.class, Controller4Impl.class);
            register(graph, Controller5.class, Controller5Impl.class);
            register(graph, Controller6.class, Controller6Impl.class);
            register(graph, Controller7.class, Controller7Impl.class);
            register(graph, Controller8.class, Controller8Impl.class);
            register(graph, Controller9.class, Controller9Impl.class);
            register(graph, Service0.class, Service0Impl.class);
            register(graph, Service1.class, Service1Impl.class);
            register(graph, Service2.class, Service2Impl.class);
            register(graph, Service3.class, Service3Impl.class);
            register(graph, Service4.class, Service4Impl.class);
            register(graph, Service5.class, Service5Impl.class);
            register(graph, Service6.class, Service6Impl.class);
            register(graph, Service7.class, Service7Impl.class);
            register(graph, Service8.class, Service8Impl.class);
            register(graph, Service9.class, Service9Impl.class);

            graph.inject(container);

        } catch (ProviderConflictException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * testFunc1.
     */
    @Test
    public void testFunc5() {
        MainAbility.Container10 container = new MainAbility.Container10();
        new MvcGraph().inject(container);
    }

    /**
     * testFunc1.
     */
    @Test
    public void testFunc6() {
        MainAbility.Container10x10 container = new MainAbility.Container10x10();
        new MvcGraph().inject(container);
    }
}