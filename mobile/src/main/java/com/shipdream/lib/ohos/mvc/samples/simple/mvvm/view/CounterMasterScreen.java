/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.samples.simple.mvvm.view;


import com.shipdream.lib.ohos.mvc.Log;
import com.shipdream.lib.ohos.mvc.NavigationManager;
import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.Toast;
import com.shipdream.lib.ohos.mvc.samples.simple.mvvm.ResourceTable;
import com.shipdream.lib.ohos.mvc.samples.simple.mvvm.controller.CounterMasterController;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.BlendMode;
import ohos.global.resource.Resource;
import ohos.utils.PacMap;

import javax.inject.Inject;

public class CounterMasterScreen extends AbstractFragment<CounterMasterController> {

    @Inject
    private NavigationManager navigationManager;


    @Override
    protected Class<CounterMasterController> getControllerClass() {
        return CounterMasterController.class;
    }

    /**
     * getLayoutResId.
     *
     * @return Layout id used to inflate the view of this MvcFragment.
     */
    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_fragment_counter_master;
    }

    private FragmentCounterMasterBinding binding;

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        binding = new FragmentCounterMasterBinding();
        binding.setLifecycle(getFractionAbility().getLifecycle());
        binding.initComponent(view);
        binding.initComponent(view);
        binding.setModel(controller.getModel());

        if (reason.isFirstTime()) {
            getFractionAbility().getUITaskDispatcher().delayDispatch(() -> {
                CounterMasterInsideView ff = new CounterMasterInsideView();
                getChildFragmentManager().beginTransaction()
                        .replace(ResourceTable.Id_fragment_a_anotherFragmentContainer, ff).commit();
            }, 200);
        }
        view.findComponentById(ResourceTable.Id_fragment_a_buttonIncrement)
                .setClickedListener(this::increment);
        view.findComponentById(ResourceTable.Id_fragment_a_buttonDecrement)
                .setClickedListener(this::decrement);
        view.findComponentById(ResourceTable.Id_fragment_a_buttonShowDetailScreen)
                .setClickedListener(this::goToDetailPage);
        view.findComponentById(ResourceTable.Id_fragment_a_ipRefresh)
                .setClickedListener(this::refreshIp);

        Image ipRefresh = (Image) view.findComponentById(ResourceTable.Id_fragment_a_ipRefresh);
        Resource imageResource = null;
        try {
            imageResource = getFractionAbility().getResourceManager().getResource(ResourceTable.Media_stat_notify_sync);
        } catch (Exception e) {
            Log.i("hh", "error" + e.getMessage());
        }
        PixelMapElement pixelMapElement = new PixelMapElement(imageResource);
        pixelMapElement.setFilterPixelMap(true);
        getTintElement(pixelMapElement, 0xff999999);
        ipRefresh.setImageElement(pixelMapElement);
    }

    private Element getTintElement(Element element, int intColor) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[2][];
        states[0] = new int[]{0};
        states[1] = new int[]{0};
        element.setStateColorList(states, colors);
        element.setStateColorMode(BlendMode.SRC_ATOP);
        return element;
    }

    private void increment(Component component) {
        controller.increment(component);
    }

    private void decrement(Component component) {
        controller.decrement(component);
    }

    private void goToDetailPage(Component component) {
        controller.goToDetailScreen(component);
    }

    private void refreshIp(Component component) {
        controller.refreshIp();
    }


    @Override
    public void update() {
    }

    private void onEvent(CounterMasterController.Event.OnHttpError event) {

        Toast.showShort(getFractionAbility(),
                String.format("Http error(%d): %s", event.getStatusCode(), event.getMessage()));
    }

    private void onEvent(CounterMasterController.Event.OnNetworkError event) {

        Toast.showShort(getFractionAbility(),
                String.format("Network error: %s", event.getIoException().getMessage()));
    }
}
