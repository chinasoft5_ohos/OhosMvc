package com.shipdream.lib.ohos.mvc.samples.simple.mvvm;

import com.shipdream.lib.ohos.mvc.FragmentController;
import com.shipdream.lib.ohos.mvc.MvcAbility;
import com.shipdream.lib.ohos.mvc.MvcFragment;
import com.shipdream.lib.ohos.mvc.samples.simple.mvvm.view.AppDelegateFragment;
import ohos.agp.utils.Color;
import ohos.utils.PacMap;

public class MainAbility extends MvcAbility {

    @Override
    protected void onCreate(PacMap pacMap) {
        super.onCreate(pacMap);
        getWindow().setStatusBarColor(Color.getIntColor("#aaaaaa"));
        getWindow().setStatusBarVisibility(0);
    }

    @Override
    protected Class<? extends MvcFragment> mapFragmentRouting(
            Class<? extends FragmentController> controllerClass) {
        String controllerPackage = controllerClass.getPackage().getName();

        //Find the classes of fragment under package .view and named in form of xxxScreen
        //For example

        //a.b.c.CounterMasterController -> a.b.c.view.CounterMasterScreen

        String viewPkgName = controllerPackage.substring(0, controllerPackage.lastIndexOf(".")) + ".view";
        String fragmentClassName = viewPkgName + "."
                + controllerClass.getSimpleName().replace("Controller", "Screen");

        try {
            return (Class<? extends MvcFragment>) Class.forName(fragmentClassName);
        } catch (ClassNotFoundException e) {
            String msg = String.format("Fragment class(%s) for controller(%s) can not be found",
                    fragmentClassName, controllerClass.getName());
            throw new RuntimeException(msg, e);
        }
    }


    @Override
    protected Class<? extends MvcAbility.DelegateFragment> getDelegateFragmentClass() {
        return AppDelegateFragment.class;
    }
}
