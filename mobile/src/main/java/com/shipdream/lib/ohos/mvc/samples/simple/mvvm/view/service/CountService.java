/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.samples.simple.mvvm.view.service;


import com.shipdream.lib.ohos.mvc.Log;
import com.shipdream.lib.ohos.mvc.MvcService;
import com.shipdream.lib.ohos.mvc.samples.simple.mvvm.ResourceTable;
import com.shipdream.lib.ohos.mvc.samples.simple.mvvm.controller.CounterServiceController;
import ohos.aafwk.content.Intent;
import ohos.event.notification.NotificationHelper;
import ohos.event.notification.NotificationRequest;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.rpc.RemoteException;

import java.io.IOException;

public class CountService extends MvcService<CounterServiceController> implements CounterServiceController.View {

    private final static int NOTIFICATION_ID = 0;
    private final static String TAG = "CountService";


    @Override
    protected Class<CounterServiceController> getControllerClass() {
        return CounterServiceController.class;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

    }

    @Override
    protected void onStop() {
        super.onStop();
        controller.stopAutoIncrement();
        try {
            NotificationHelper.cancelNotification(NOTIFICATION_ID);
        } catch (RemoteException e) {
            Log.e(TAG, "cancelNotification - " + e.getMessage());
        }
    }

    public void onTaskRemoved(Intent rootIntent) {
        terminateAbility();
    }


    @Override
    protected void onCommand(Intent intent, boolean restart, int startId) {
        super.onCommand(intent, restart, startId);
        controller.startAutoIncrement();
    }

    private void updateNotification(int currentCount) {

        NotificationRequest notificationRequest = new NotificationRequest(NOTIFICATION_ID);
        Resource resource = null;
        try {
            resource = getResourceManager().getResource(ResourceTable.Media_ic_notification_auto_count);
            ImageSource imageSource = ImageSource.create(resource, new ImageSource.SourceOptions());
            PixelMap pixelmap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
            notificationRequest.setLittleIcon(pixelmap);
        } catch (IOException e) {
            Log.e(TAG, "setLittleIcon - " + e.getMessage());
        } catch (NotExistException e) {
            Log.e(TAG, "setLittleIcon - " + e.getMessage());
        }
        NotificationRequest.NotificationLongTextContent textContent =
                new NotificationRequest.NotificationLongTextContent();
        textContent.setTitle("Count to 10");
        textContent.setText("Current count: " + currentCount);
        NotificationRequest.NotificationContent notificationContent =
                new NotificationRequest.NotificationContent(textContent);
        notificationRequest.setContent(notificationContent);

        try {
            NotificationHelper.publishNotification(notificationRequest);
        } catch (RemoteException e) {
            Log.e(TAG, "updateNotification - " + e.getMessage());
        }
    }

    @Override
    public void counterFinished() {
        terminateAbility();

    }

    @Override
    public void update() {
        updateNotification(controller.getCount());
    }

}