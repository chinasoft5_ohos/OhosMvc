/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shipdream.lib.ohos.mvc.samples.simple.mvvm.view;

import com.shipdream.lib.ohos.mvc.samples.simple.mvvm.ResourceTable;
import com.shipdream.lib.ohos.mvc.samples.simple.mvvm.controller.CounterMasterController;
import ohos.aafwk.ability.Lifecycle;
import ohos.agp.components.Component;
import ohos.agp.components.RoundProgressBar;
import ohos.agp.components.Text;

/**
 * FragmentCounterMasterBinding.
 *
 * @author a
 * @version 1.0.0
 */
public class FragmentCounterMasterBinding {
    private Text counterDisplay;
    private Text ipValue;
    private RoundProgressBar roundProgressBar;
    private CounterMasterController.Model model;
    private Lifecycle owner;

    /**
     * initComponent .
     *
     * @param component Component
     */
    public void initComponent(Component component) {
        counterDisplay = (Text) component
                .findComponentById(ResourceTable.Id_fragment_a_counterDisplay);
        ipValue = (Text) component.findComponentById(ResourceTable.Id_fragment_a_ipValue);
        roundProgressBar = (RoundProgressBar) component
                .findComponentById(ResourceTable.Id_fragment_a_ipProgress);
    }

    /**
     * setLifecycle .
     *
     * @param owner Lifecycle
     */
    public final void setLifecycle(Lifecycle owner) {
        this.owner = owner;
    }

    /**
     * getLifecycle .
     *
     * @return Lifecycle
     */
    public Lifecycle getLifecycle() {
        return owner;
    }

    /**
     * setModel .
     *
     * @param arg CounterMasterController
     */
    public void setModel(CounterMasterController.Model arg) {
        model = arg;
        model.setModelUpdateListener(model -> {
            counterDisplay.setText(model.getCount());
            roundProgressBar.setVisibility(model.getProgressVisible() ?
                    Component.VISIBLE : Component.INVISIBLE);
            ipValue.setText(model.getIpAddress());
        });
    }
}
