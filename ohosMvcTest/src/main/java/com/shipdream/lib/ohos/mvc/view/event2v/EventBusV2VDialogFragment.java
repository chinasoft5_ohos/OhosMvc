/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.event2v;


import com.shipdream.lib.ohos.mvc.MvcDialogFragment;
import com.shipdream.lib.ohos.mvc.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

public class EventBusV2VDialogFragment extends MvcDialogFragment {
    private Text textView;

    public EventBusV2VDialogFragment(Context context) {
        super(context);
    }

    @Override
    public Component onCreateView(LayoutScatter inflater) {
        setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        setAutoClosable(true);

        Component view = inflater.parse(ResourceTable.Layout_fragment_mvc_v2v_dialog, null, false);
        textView = (Text) view.findComponentById(ResourceTable.Id_fragment_mvc_v2v_dialog_text);
        Component button = view.findComponentById(ResourceTable.Id_fragment_mvc_v2v_dialog_button);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                postEvent2V(new Events.OnFragmentTextChanged("Dialog Closed"));
                destroy();
            }
        });
        return view;
    }

    private void onEvent(Events.OnDialogButtonChanged onButtonUpdated) {
        textView.setText(onButtonUpdated.getText());
    }

}
