/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.viewpager;


import com.shipdream.lib.ohos.mvc.NavigationManager;
import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.ResourceTable;
import com.shipdream.lib.ohos.mvc.view.help.LifeCycleMonitor;
import com.shipdream.lib.ohos.mvc.view.help.LifeCycleMonitorA;
import com.shipdream.lib.ohos.mvc.view.viewpager.controller.SecondFragmentController;
import com.shipdream.lib.ohos.mvc.view.viewpager.controller.TabControllerA;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.utils.PacMap;

import javax.inject.Inject;

public class TabFragmentA extends BaseTabFragment<TabControllerA> {
    private Text textView;

    @Inject
    private NavigationManager navigationManager;

    @Override
    protected Class getControllerClass() {
        return TabControllerA.class;
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_fragment_view_pager_tab;
    }

    @Inject
    private LifeCycleMonitorA lifeCycleMonitorA;

    @Override
    protected LifeCycleMonitor getLifeCycleMonitor() {
        return lifeCycleMonitorA;
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);

        textView = (Text) view.findComponentById(ResourceTable.Id_fragment_view_pager_tab_text);
        textView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                navigationManager.navigate(component).to(SecondFragmentController.class);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void update() {
        textView.setText(controller.getModel().getName());
    }

    public void setText() {
        textView.setText(TabControllerA.RESTORE_TEXT);
    }
}
