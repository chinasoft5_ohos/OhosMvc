/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.lifecycle;


import com.shipdream.lib.ohos.mvc.*;
import com.shipdream.lib.ohos.mvc.view.help.LifeCycleMonitor;
import ohos.agp.components.Component;
import ohos.utils.PacMap;

import javax.inject.Inject;

public class MvcTestFragment extends MvcFragment<MvcTestFragment.Controller> {
    public static class Controller extends FragmentController {
        @Override
        public Class modelType() {
            return null;
        }
    }

    @Inject
    private LifeCycleMonitor lifeCycleMonitor;

    @Override
    protected Class<Controller> getControllerClass() {
        return Controller.class;
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_fragment_mvc_test;
    }

    @Override
    public void onCreate(PacMap savedInstanceState) {
        super.onCreate(savedInstanceState);
        lifeCycleMonitor.onCreate(savedInstanceState);
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        lifeCycleMonitor.onCreateView(view, savedInstanceState);
        lifeCycleMonitor.onViewCreated(view, savedInstanceState);
        super.onViewReady(view, savedInstanceState, reason);
        lifeCycleMonitor.onViewReady(view, savedInstanceState, reason);
    }

    @Override
    public void onResume() {
        super.onResume();
        lifeCycleMonitor.onResume();
    }

    @Override
    protected void onReturnForeground() {
        super.onReturnForeground();
        lifeCycleMonitor.onReturnForeground();
    }

    @Override
    protected void onPushToBackStack() {
        super.onPushToBackStack();
        if (lifeCycleMonitor != null) {
            lifeCycleMonitor.onPushToBackStack();
        }
    }

    @Override
    protected void onPopAway() {
        super.onPopAway();
        lifeCycleMonitor.onPopAway();
    }

    @Override
    protected void onPoppedOutToFront() {
        super.onPoppedOutToFront();
        lifeCycleMonitor.onPoppedOutToFront();
    }

    @Override
    protected void onOrientationChanged(int lastOrientation, int currentOrientation) {
        super.onOrientationChanged(lastOrientation, currentOrientation);
        lifeCycleMonitor.onOrientationChanged(lastOrientation, currentOrientation);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lifeCycleMonitor.onDestroyView();
    }

    @Override
    public void onDestroy() {
        lifeCycleMonitor.onDestroy();
        super.onDestroy();
    }

    @Override
    public void update() {

    }
}
