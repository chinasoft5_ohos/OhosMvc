package com.shipdream.lib.ohos.mvc;

import com.shipdream.lib.ohos.mvc.event.bus.EventBus;
import com.shipdream.lib.ohos.mvc.event.bus.annotation.EventBusV;
import ohos.agp.utils.Color;
import ohos.utils.PacMap;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class TestAbility extends MvcAbility {

    public interface Event {
        class OnFragmentsResumed {
            public String sender;

            public OnFragmentsResumed(String sender) {
                this.sender = sender;
            }
        }
    }

    public enum State {
        CREATE,
        RESUME,
        RESUME_FRAGMENTS,
        PAUSE,
        SAVE_INSTANCE_STATE,
        DESTROY
    }

    public static class Proxy {
        protected void onCreate() {
        }

        protected void onResume() {
        }

        protected void onResumeFragments() {
        }

        protected void onPause() {
        }

        protected void onSaveInstanceState() {
        }

        protected void onDestroy() {
        }
    }

    @Inject
    @EventBusV
    private EventBus eventBusV;

    private State state;

    public State getMvState() {
        return state;
    }

    private List<Proxy> proxies = new CopyOnWriteArrayList<>();
    ;

    public DelegateFragment getDelegateFragment() {
        return delegateFragment;
    }

    public void addProxy(Proxy proxy) {
        synchronized (proxies) {
            proxies.add(proxy);
        }
    }

    public void removeProxy(Proxy proxy) {
        synchronized (proxies) {
            proxies.remove(proxy);
        }
    }

    @Override
    protected void onCreate(PacMap savedInstanceState) {
        getWindow().setStatusBarColor(Color.getIntColor("#757575"));
        getWindow().setStatusBarVisibility(0);
        Mvc.graph().inject(this);

        super.onCreate(savedInstanceState);
        state = State.CREATE;
        for (Proxy proxy : proxies) {
            proxy.onCreate();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        state = State.RESUME;
        for (Proxy proxy : proxies) {
            proxy.onResume();
        }
    }


    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);

        state = State.SAVE_INSTANCE_STATE;
        for (Proxy proxy : proxies) {
            proxy.onSaveInstanceState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        state = State.PAUSE;
        for (Proxy proxy : proxies) {
            proxy.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        state = State.DESTROY;
        for (Proxy proxy : proxies) {
            proxy.onDestroy();
        }

        Mvc.graph().release(this);
    }
}
