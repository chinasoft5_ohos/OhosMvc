/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.nav;


import com.shipdream.lib.ohos.mvc.*;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.utils.PacMap;

import javax.inject.Inject;

public abstract class NavFragment extends MvcFragment {
    private Button next;
    private Button clear;
    private Text textView;


    @Inject
    private NavigationManager navigationManager;

    @Inject
    private AnotherController anotherPresenter;

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_fragment_mvc_test_nav;
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);

        textView = (Text) view.findComponentById(ResourceTable.Id_text);
        textView.setText(getClass().getSimpleName());

        next = (Button) view.findComponentById(ResourceTable.Id_button_next);
        next.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                boolean interim = false;
                if (getNextFragmentLocId() != null) {
                    interim = getNextFragmentLocId().getName().contains("C");
                }
                navigationManager.navigate(component).to(getNextFragmentLocId(),
                        new Forwarder().setInterim(interim));
            }
        });

        clear = (Button) view.findComponentById(ResourceTable.Id_button_clear);
        clear.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                navigationManager.navigate(component).back(null);
            }
        });
        anotherPresenter.populateData();
    }

    @Override
    public void onResume() {
        super.onResume();
        Text title = (Text) getComponent().findComponentById(ResourceTable.Id_title);
        title.setText("Page: " + getClass().getSimpleName());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected abstract Class<? extends Controller> getNextFragmentLocId();
}
