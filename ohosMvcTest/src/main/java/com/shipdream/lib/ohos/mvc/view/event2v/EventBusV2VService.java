/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.event2v;

import com.shipdream.lib.ohos.mvc.MvcService;
import ohos.aafwk.content.Intent;
import ohos.rpc.IRemoteObject;

public class EventBusV2VService extends MvcService {

    @Override
    protected Class getControllerClass() {
        return null;
    }

    @Override
    public void update() {

    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
        super.onCommand(intent, restart, startId);
        postEvent2V(new Events.OnFragmentTextChanged("Updated By Service"));
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        super.onConnect(intent);
        return null;
    }

    @Override
    public void onDisconnect(Intent intent) {
        super.onDisconnect(intent);
    }

}
