/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.event2v;


import com.shipdream.lib.ohos.mvc.MvcFragment;
import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.ResourceTable;
import com.shipdream.lib.ohos.mvc.view.event2v.controller.V2VTestController;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.utils.PacMap;

public class EventBusV2VFragment extends MvcFragment<V2VTestController> implements V2VTestController.View {
    private Text textView;
    private Component buttonDialog;
    private Component buttonService;

    @Override
    protected Class<V2VTestController> getControllerClass() {
        return V2VTestController.class;
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_fragment_mvc_v2v;
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);

        textView = (Text) view.findComponentById(ResourceTable.Id_fragment_mvc_v2v_text);

        buttonDialog = view.findComponentById(ResourceTable.Id_fragment_mvc_v2v_btnDialog);
        buttonDialog.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                final EventBusV2VDialogFragment cityDialog = new EventBusV2VDialogFragment(getFractionAbility());
                cityDialog.showDialogFragment();
            }
        });


        buttonService = view.findComponentById(ResourceTable.Id_fragment_mvc_v2v_btnService);
        buttonService.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getFractionAbility().getBundleName())
                        .withAbilityName(EventBusV2VService.class.getCanonicalName())
                        .build();
                intent.setOperation(operation);
                getFractionAbility().startAbility(intent);
            }
        });
    }

    private void onEvent(Events.OnFragmentTextChanged event) {
        textView.setText(event.getText());
    }

    @Override
    public void updateDialogButton(String text) {
        postEvent2V(new Events.OnDialogButtonChanged(text));
    }

    @Override
    public void update() {

    }
}
