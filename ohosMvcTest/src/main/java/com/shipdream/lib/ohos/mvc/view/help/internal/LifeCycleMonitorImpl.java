/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.help.internal;


import com.shipdream.lib.ohos.mvc.Log;
import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.view.help.LifeCycleMonitor;
import ohos.agp.components.Component;
import ohos.utils.PacMap;

public class LifeCycleMonitorImpl implements LifeCycleMonitor {


    @Override
    public void onCreate(PacMap savedInstanceState) {
        Log.info("Lifecycle method invoked: onCreated");
    }

    @Override
    public void onCreateView(Component rootComponent, PacMap savedInstanceState) {
        Log.info("Lifecycle method invoked: onCreateComponent");
    }

    @Override
    public void onViewCreated(Component rootComponent, PacMap savedInstanceState) {
        Log.info("Lifecycle method invoked: onComponentCreated");
    }

    @Override
    public void onViewReady(Component rootComponent, PacMap savedInstanceState, Reason reason) {
        Log.info("Lifecycle method invoked: onComponentReady, reason?: " + reason.toString());
    }

    @Override
    public void onResume() {
        Log.info("Lifecycle method invoked: onResume");
    }

    @Override
    public void onReturnForeground() {
        Log.info("Lifecycle method invoked: onReturnForeground");
    }

    @Override
    public void onPushToBackStack() {
        Log.info("Lifecycle method invoked: onPushToBackStack");
    }

    @Override
    public void onPopAway() {
        Log.info("Lifecycle method invoked: onPopAway");
    }

    @Override
    public void onPoppedOutToFront() {
        Log.info("Lifecycle method invoked: onPoppedOutToFront");
    }

    @Override
    public void onOrientationChanged(int lastOrientation, int currentOrientation) {
        Log.info("Lifecycle method invoked: onOrientationChanged");
    }

    @Override
    public void onDestroyView() {
        Log.info("Lifecycle method invoked: onDestroyComponent");
    }

    @Override
    public void onDestroy() {
        Log.info("Lifecycle method invoked: onDestroy");
    }
}
