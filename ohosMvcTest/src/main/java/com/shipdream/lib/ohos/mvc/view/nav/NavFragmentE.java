/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shipdream.lib.ohos.mvc.view.nav;


import com.shipdream.lib.ohos.mvc.MvcFragment;
import com.shipdream.lib.ohos.mvc.Reason;
import com.shipdream.lib.ohos.mvc.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.utils.PacMap;

import javax.inject.Inject;

public class NavFragmentE extends MvcFragment {
    @Inject
    private ControllerE presenterE;

    @Inject
    private ControllerG presenterG;

    private Text textView;

    @Override
    protected Class getControllerClass() {
        return null;
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_fragment_mvc_test_nav_e;
    }

    @Override
    public void onViewReady(Component view, PacMap savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        textView = (Text) view.findComponentById(ResourceTable.Id_nav_frag_e_text);

        textView.setText(presenterE.getValue());
    }

    @Override
    public void update() {

    }
}
