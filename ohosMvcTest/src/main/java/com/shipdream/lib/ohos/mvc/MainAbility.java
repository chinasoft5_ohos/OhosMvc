package com.shipdream.lib.ohos.mvc;

import com.shipdream.lib.ohos.mvc.view.event2v.EventBusV2VAbility;
import com.shipdream.lib.ohos.mvc.view.injection.InjectionTestAbility;
import com.shipdream.lib.ohos.mvc.view.injection.InjectionTestAbilityStateManagedObjects;
import com.shipdream.lib.ohos.mvc.view.lifecycle.MvcTestAbility;
import com.shipdream.lib.ohos.mvc.view.nav.MvcTestAbilityNavigation;
import com.shipdream.lib.ohos.mvc.view.viewpager.ViewPagerTestAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

import java.util.HashMap;
import java.util.Map;

public class MainAbility extends Ability implements Component.ClickedListener {

    private Map<Integer, String> demoMap = new HashMap<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(Color.BLACK.getValue());
        getWindow().setStatusBarVisibility(0);
        findComponentById(ResourceTable.Id_btn1).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn3).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn4).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn5).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn6).setClickedListener(this);
        demoMap.put(ResourceTable.Id_btn1, EventBusV2VAbility.class.getCanonicalName());
        demoMap.put(ResourceTable.Id_btn3, InjectionTestAbilityStateManagedObjects.class.getCanonicalName());
        demoMap.put(ResourceTable.Id_btn4, MvcTestAbility.class.getCanonicalName());
        demoMap.put(ResourceTable.Id_btn5, MvcTestAbilityNavigation.class.getCanonicalName());
        demoMap.put(ResourceTable.Id_btn6, ViewPagerTestAbility.class.getCanonicalName());

    }

    @Override
    public void onClick(Component component) {
        int id = component.getId();
        if (demoMap.containsKey(id)) {
            startDemoAbility(demoMap.get(id));
        }
    }

    private void startDemoAbility(String abilityName) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
