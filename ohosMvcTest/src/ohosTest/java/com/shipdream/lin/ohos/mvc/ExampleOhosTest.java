/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shipdream.lin.ohos.mvc;

import com.shipdream.lib.ohos.mvc.*;
import org.junit.Test;


/**
 * ExampleOhosTest .
 *
 * @author a
 * @version v
 */
public class ExampleOhosTest {

    @Test
    public void testSetRootComponent() {
        MvcComponent testComponent = new MvcComponent("TestComponent");
        MvcGraph graph = Mvc.graph();
        graph.setRootComponent(testComponent);
    }

    @Test
    public void testClearMonitors() {
        MvcGraph graph = Mvc.graph();
        graph.clearMonitors();
    }

    @Test
    public void testClearDereferencedListeners() {
        MvcGraph graph = Mvc.graph();
        graph.clearDereferencedListeners();
    }

    @Test
    public void testInject() {
        MvcGraph graph = Mvc.graph();
        MvcComponent testComponent = new MvcComponent("TestComponent");
        graph.inject(testComponent);
    }
}